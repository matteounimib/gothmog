import os
from flask import Blueprint, Flask, flash, request, redirect, render_template, session, Markup, Response
from flask import current_app as app
import urllib.request

from libs.query import *

query_bp = Blueprint('query_bp', __name__, template_folder='templates', static_folder='static')

@app.route('/query', methods=['GET'])
def query_form():
    # Used to save query result
    session['query'] = []
    #Save common word in the file temp.txt
    common_file(session['models'][0], session['models'][1])

    return render_template('./query.html')

#Apply the query on the two selected model
@app.route('/query', methods=['GET','POST'])
def query():
    word = request.form.get('autocomplete')
    par = request.form.get('par')
    model = session['models']
    options = request.form['query']

    f = open('./temp.txt', "r")
    common = f.read()
    common = common.split('\n')
    f.close()

    # Give an error if the user doesn't write a word
    if word == '':
        flash('Write a word')
        return render_template('./query.html', query=session['query'])

    if word in common:
        # Global Distance
        if options == 'Global':
            result = global_distance(model[0], model[1], word)
            session['query'] += [['Global', word, round(result, 6)]]
        # Local Distance
        if options == 'Local':

            # check if par is an int
            try:
                par = int(par)
            except:
                flash('Insert an integer')
                return render_template('./query.html',
                                query=session['query'])

            # check if par is bigger than the common words between the two model
            if par > len(common):
                flash(
                    'The parameter is bigger than the common words between the two model')
                return render_template('./query.html', query=session['query'])

            result = local_neighbour_measure(
                model[0], model[1], word, par, common)
            session['query'] += [['Local (n=' + str(par) + ')',
                                  word, round(result, 6)]]

        # Cosine distance
        if options == 'Cos':
            result = cosine_sim(model[0], model[1], word)
            session['query'] += [['Cosine', word, round(result, 6)]]

    else:
        flash('This word is not in common between the two model')

    return render_template('./query.html', query=session['query'])
