import re
import codecs
from flask import Blueprint, Flask, flash, request, redirect, render_template, send_file, session, Markup, Response
from flask import current_app as app

import urllib.request
from libs.preprocessing import *

preprocessing_bp = Blueprint('preprocessing_bp', __name__, template_folder='templates', static_folder='static')

@app.route('/preprocessing')
def form_preprocessing():
    try:
        return render_template("./preprocessing.html", data=session['data'])
    except:
        return render_template("./preprocessing.html", data=False, regexp=False)

#Function for the all the preprocessing function
def appPre(f, content, check, string):
    content = f(content)
    check.append(string)

def testPre(f, content):
    return f(content)

@app.route('/preprocessing', methods=['GET', 'POST'])
def preprocessing():
    data = session['data']
    text_list = request.form.getlist('input_text[]')
    regexp = []
    #Check if the regexp are WFF. If not give a list of which one are not WFF
    if text_list != []:
        not_wff = ''
        cont = 0

        for i in range(len(text_list)):

            if i == len(text_list) and check_reg(text_list[i]) == False:
                not_wff += str(text_list[i]) + ' '
                cont += 1

            elif i != len(text_list) and check_reg(text_list[i]) == False:
                not_wff += str(text_list[i]) + ', '
                cont += 1

            elif text_list[i] != '':
                regexp.append(text_list[i])

        if cont > 1:
            flash('The ' + not_wff + 'formulas are not WFF.')

            return render_template('./preprocessing.html', data=data, regexp=regexp)
        elif cont == 1:
            flash('The ' + not_wff + 'formula is not WFF.')

            return render_template('./preprocessing.html', data=data, regexp=regexp)

    checked = request.form.getlist('Preproc')
    if request.form['ls'] != 'none':
        checked.append(str(request.form['ls']))
    check = []

    #Apply the given regexp to the two files
    for i in data:
        content = ""
        with open('./uploads/' + i, encoding = 'utf-8') as f:
            for line in f:
                content += line
        content.strip()
        f = codecs.open("./uploads/" + i + ".prep", "w+", "utf-8")
        #Dictonary of all regexp function
        functs = {'Pare':[parenthesis_removal, 'Parenthesis and Symbol'],
                  'Email':[email_removal, 'Email'],
                  'Stop':[stopWordsRemoval, 'Stop Words'],
                  'Alpha':[nonalphanumeric_removal, 'Nonalphanumeric'],
                  'stemming':[stemming, 'Stemming'],
                  'lemmatize':[lemmatize, 'Lemmatize']}

        #Punctuation removal
        content = " ".join(punctuation_removal(i) for i in content.split())
        #Apply the given default regexp
        for i in checked:
            if i == 'length_cut':
                try:
                    if int(request.form.get('par')) >= 0:
                        content = length_cut(content, int(request.form.get('par')))
                        if 'Minimum length (' + request.form.get('par') + ')' not in check:
                            check.append('Minimum length (' + request.form.get('par') + ')')
                    
                    else:
                        flash('Insert a non negative number')

                        return render_template('./preprocessing.html', data=data, regexp=regexp)

                except:
                    flash('Insert a non negative number')

                    return render_template('./preprocessing.html', data=data, regexp=regexp)

            else:
                appPre(functs[i][0], content, check, functs[i][1])
                content = testPre(functs[i][0], content)
        #Apply the manual regexp
        for i in text_list:
            content = manual_regexp(content, i)

        content = " ".join(punctuation_removal(i) for i in content.split())
        f.write(content.lower())
        f.close()

    #Create a session to save all the regexp selected and add the new one
    try:
        session['regexp'] += check + regexp
        session['regexp'] = list(set(session['regexp']))
    except:
        session['regexp'] = check + regexp

    session['data'] = list(map(lambda x: x + ".prep", session['data']))

    try:
        strreg = 'The regexps and methods applied are: ' + str(session['regexp'][0])
        for i in session['regexp'][1:]:
            strreg += ', ' + str(i)

        message = Markup('Preprocessing complete.<br/>' + strreg + \
            '. <br/> <br/>Now go to <a href="/preprocessing/frequency">' \
                '<label class="etichetta">frequency</label>')
        flash(message)
    except:

        session['regexp_list'] = []
        flash(Markup('<br/>Now go to <a href="/preprocessing/frequency">\
        <label class="etichetta">frequency</label>'))

        return render_template('./preprocessing.html', data=data, regexp=False)
    session['regexp_list'] = session['regexp']

    return render_template('./preprocessing.html', data=data, regexp=regexp)

