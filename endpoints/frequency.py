import os
import codecs
import urllib.request
from flask import Blueprint, Flask, flash, request, redirect, render_template, send_file, session, Markup, Response
from flask import current_app as app
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure

from libs.preprocessing import fqCounter, fqRemove
from libs.graphUtils import *

frequency_bp = Blueprint('frequency_bp', __name__, template_folder='templates', static_folder='static')


@app.route('/plot.png')
def plot_png():

    return plot_generic(create_figure)

#Create the plot image
def create_figure():
    files = session['data']
    allText = ""
    for fi in files:
        location = os.path.join(app.config['UPLOAD_FOLDER'], fi)
        with open(location, encoding = 'utf-8') as f:
            allText += allText+f.read()
    data = graphPrepare(fqCounter(allText))
    fig = Figure()
    fig.patch.set_alpha(0)
    axis = fig.add_subplot(1, 1, 1)
    axis.plot(data[0], data[1])
    axis.set_xlim(data[0][0], 0)
    axis.set_xlabel("Word frequency")
    axis.set_ylabel("Cumulative number of words")
    axis.grid()
    return fig


@app.route('/preprocessing/frequency')
def frequency():
    files = session['data']
    for fi in files:
        location = os.path.join(app.config['UPLOAD_FOLDER'], fi)
        with open(location, encoding = 'utf-8') as f:
            text = f.read()
            if text == "":
                return render_template("./frequencyError.html", value=False)
    return render_template("./frequency.html")

#Given a frequency, it remove the words that are lower 
@app.route('/preprocessing/frequency', methods=['POST'])
def frequencyRemoval():
    files = session['data']
    try:
        minFreq = float(request.form.get('minFreq'))
        if (minFreq > 1) or (minFreq < 0):
            return render_template("./frequencyError.html", value=True)
        for fi in files:
            location = os.path.join(app.config['UPLOAD_FOLDER'], fi)
            with open(location, encoding = 'utf-8') as f:
                text = f.read()
                text = fqRemove(text, minFreq)
            with open(os.path.join(location + ".fq"), 'w', encoding = 'utf-8') as f:
                f.write(text)

        flash("Word removal complete".format(
            request.form.get('minFreq')) + " now go")
        session['data'] = list(map(lambda x: x + ".fq", session['data']))
        flash(Markup(
            'to <a href="/preprocessingStats"><label class="etichetta">Preprocessing Stats</label></a>'))

        flash(Markup(
            ' or go to <a href="/model"><label class="etichetta">Train model</label>'))

        for fi in files:
            location = os.path.join(app.config['UPLOAD_FOLDER'], fi)
            with open(location, encoding = 'utf-8') as f:
                text = f.read()
                if text == "":
                    return render_template("./frequencyError.html", value=False)
        return render_template("./frequency.html", value=False)
    except:

        return render_template("./frequencyError.html", value=True)
