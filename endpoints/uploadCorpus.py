import os
from flask import Blueprint, Flask, flash, request, redirect, render_template, send_file, session, Response, Markup
from flask import current_app as app
from werkzeug.utils import secure_filename

import urllib.request

upload_bp = Blueprint('upload_bp', __name__, template_folder='templates', static_folder='static')

ALLOWED_EXTENSIONS = set(['txt'])
try:
    os.mkdir('./uploads/')
except:
    pass


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/upload')
def upload_form():
    return render_template('uploadCorpus.html')


@app.route('/upload', methods=['POST'])
def upload_file():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No file selected')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], file.filename))
            flash(Markup('Upload completed, now go to <a href="/select">Select Corpus</a>'))
            return redirect(request.url)
        else:
            flash('Allowed file type is txt')
            return redirect(request.url)
