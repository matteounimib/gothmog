from flask import Blueprint, Flask, flash, request, redirect, render_template, session, Markup, Response
from flask import current_app as app
import urllib.request
import codecs

from libs.inducer import *

trainModel_bp = Blueprint('trainModel_bp', __name__, template_folder='templates', static_folder='static')

#Use the two preprocessed .txt files to train the two model
@app.route('/trainmodel', methods=['GET', 'POST'])
def train_model():
    try:
        # We save the models in other variables
        session['models'] = session['data']
        data = session['models']
        if request.method == 'POST':
            f = codecs.open("./uploads/" + data[0], "r", 'utf-8')
            first = f.read()
            f.close()
            f = codecs.open("./uploads/" + data[1], "r", 'utf-8')
            second = f.read()
            f.close()
            compass = first + second
            f = codecs.open('./uploads/compass.txt', "w+", 'utf-8')
            f.write(compass)
            f.close()
            training_twec(
                './uploads/' + data[0], './uploads/' + data[1], './uploads/compass.txt')

            session['models'] = list(
                map(lambda x: x.split(".fq")[0] + '.model', session['models']))
            print(session['models'])
            flash('You have trained successfully')
            flash(Markup('Now go to make some \
            <a href="/query"><label class="etichetta">query</label>'))

            return render_template('./trainmodel.html', data=data)

        return render_template('./trainmodel.html', data=data)

    except:
        return render_template('./trainmodel.html', data=False)
