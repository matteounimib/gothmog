import os
import urllib.request
from flask import Blueprint, Flask, flash, request, redirect, render_template, send_file, session, Markup, Response
from flask import current_app as app
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure

from libs.preprocessing import fqCounter, fqRemove
from libs.graphUtils import *

prepStats_bp = Blueprint('prepStats_bp', __name__, template_folder='templates', static_folder='static')

@app.route('/preprocessingStats')
def preprocessingStats():
    files = session['data']
    regexp = session['regexp_list']

    try:
        strreg = 'The regexps and methods applied are: ' + str(regexp[0])
        for i in regexp[1:]:
            strreg += ', ' + str(i)

        flash(Markup(strreg))
    except:
        pass

    return render_template("./preprocessingStats.html")

# Filesize plot in byte
@app.route('/size.png')
def plot_size():
    return plot_generic(create_filesize)


def create_filesize():
    processed_files = session['data']
    r = ret_data(app.config['UPLOAD_FOLDER'], processed_files,
                 lambda x: int(os.path.getsize(x)/1024))

    return graph_filesize(processed_files, r[0], r[1], r[2], "File size", "KB")

# TotWord plot
@app.route('/totWords.png')
def plot_totWords():
    return plot_generic(create_totWords)

# Tokenized word reader
def word_reader(fl):
    words = []
    with open(fl, 'r', encoding = 'utf-8') as f:
        words = nltk.word_tokenize(f.read())

    return words

# Prepare data to plot
def create_totWords():
    processed_files = session['data']
    r = ret_data(app.config['UPLOAD_FOLDER'], processed_files,
                 lambda x: len(word_reader(x)))

    return graph_filesize(processed_files, r[0], r[1], r[2], "Number of Words", "Words")

# Plot of single words
@app.route('/totSingleWords.png')
def plot_totSingleWords():
    return plot_generic(create_totSingleWords)


def create_totSingleWords():
    processed_files = session['data']
    r = ret_data(app.config['UPLOAD_FOLDER'], processed_files,
                 lambda x: len(set(word_reader(x))))

    return graph_filesize(processed_files, r[0], r[1], r[2], "Number of Single Words", "Single Words")

