#from flask import Blueprint, render_template
from flask import Blueprint, Flask, flash, request, redirect, render_template, send_file, session, Markup, Response
from flask import current_app as app
import os
import urllib.request

select_bp = Blueprint('select_bp', __name__, template_folder='templates', static_folder='static')

@app.route('/select')
def select():
    path = os.path.join(app.config['UPLOAD_FOLDER'])
    files = os.listdir(path)

    return render_template('selectCorpus.html', files=files)


@app.route('/select', methods=['POST'])
def selection():
    path = os.path.join(app.config['UPLOAD_FOLDER'])
    files = os.listdir(path)
    first = request.form['first']
    second = request.form['second']
    if first == second:
        flash('Select two different corpus')
        return redirect(request.url)
    data = [first, second]
    session['data'] = data

    flash('You have successfully selected two files')
    flash(Markup('Now go to \
    <a href="/preprocessing"><label class="etichetta">preprocessing</label>'))

    return render_template('selectCorpus.html', files=files)

