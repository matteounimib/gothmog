#from flask import Blueprint, render_template
from flask import Blueprint, Flask, flash, request, redirect, render_template, send_file, session, Markup, Response
from flask import current_app as app
import urllib.request

index_bp = Blueprint('index_bp', __name__, template_folder='templates', static_folder='static')

@app.route('/')
def homepage():
    session['regexp'] = []
    try:
        return render_template('homepage.html')
    except:
        return render_template('homepage.html', data=data)

