import os
from flask import Blueprint, Flask, flash, request, redirect, render_template, session, Markup, Response
from flask import current_app as app
import urllib.request

from libs.query import *

rank_bp = Blueprint('rank_bp', __name__, template_folder='templates', static_folder='static')

# metafunction to apply different distance measures
def appF(f, models, arg, n):
    return f(models[0], models[1], arg, n)

# Take the first or the bottom n values from a dict
def take(data, n, reverse=False):
    return dict(islice({k:v for k,v in sorted(data.items(), key = lambda i : i[1],
                                                         reverse=reverse)}.items(), n,))
@app.route('/rank', methods=['GET'])
def rankView():
    return render_template('./rank.html')

@app.route('/rank', methods=['POST'])
def rank():
    session['common'] = common_words(
        session['models'][0], session['models'][1])
    try:
        n = int(request.form['n'])
        if n <= 0:
            flash("n must be greater than 0")
            return render_template('./rank.html')
        else:
            # common distances dict
            dists = {'csim': cos_sim,
                     'global': cos_dis,}
            try:
                if request.form['distance'] in dists:
                    # apply local distance and cosine similarity
                    d = appF(dists[request.form['distance']], session['models'], session['common'], n)
                elif request.form['distance'] == 'local':
                    try:
                        # integer parsing of local neighbours
                        par = int(request.form['par'])
                    except:
                        flash("Neighbours number must be an integer number")
                        return render_template('./rank.html')
                    if par <= 0:
                        flash('Neighbours distance must be grater than 0')
                        # apply local distance
                    d = loc_dis(session['models'][0], session['models'][1],
                                session['common'], par, n)
                # take first n and bottom n dict {key: values}
                
                if request.form['distance'] == 'csim':
                    session['top'] = take(d, n, True)
                    session['bottom'] = take(d, n, False)
                
                else:
                    session['top'] = take(d, n, False)
                    session['bottom'] = take(d, n, True)
                

                return render_template('./rank.html', top=session['top'], bottom=session['bottom'])
            except:
                flash('Enter a valid type of distance value')
    except:
        flash("Enter an integer number")
        return render_template('./rank.html')
