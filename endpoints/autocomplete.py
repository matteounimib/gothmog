import os
from flask import Blueprint, Flask, flash, request, redirect, render_template, session, Markup, Response, jsonify
from flask import current_app as app
import urllib.request

from libs.query import *

autocomplete_bp = Blueprint('autocomplete_bp', __name__, template_folder='templates', static_folder='static')

#autocomplete word for the query pages
@app.route('/autocomplete', methods=['GET'])
def autocomplete():
    search = request.args.get('q')
    f = open('./temp.txt', "r")
    commons= f.read()
    commons = commons.split('\n')
    f.close()
    results = [i for i in commons if i.startswith(str(search))][:15]
    session['result'] = results
    return jsonify(matching_results=results)