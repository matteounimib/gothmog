import os
from flask import Blueprint, Flask, flash, request, redirect, render_template, send_file, session, Markup, Response
from flask import current_app as app
import urllib.request

modelSelection_bp = Blueprint('modelSelection_bp', __name__, template_folder='templates', static_folder='static')

@app.route('/model')
def model():
    return render_template('model.html')


@app.route('/selectmodel')
def selection_model():
    path = os.path.join(app.config['MODEL_FOLDER'])
    files = os.listdir(path)
    for i in files:
        if i.rsplit('.', 1)[1].lower() != 'model':
            files.remove(i)

    return render_template('selectmodel.html', files=files)


@app.route('/selectmodel', methods=['POST'])
def select_model():
    path = os.path.join(app.config['MODEL_FOLDER'])
    files = os.listdir(path)
    first = request.form['first_model']
    second = request.form['second_model']
    if first == second:
        flash('Select two different corpus')
        return redirect(request.url)
    data = [first, second]
    session['models'] = data

    flash('You have selected two model successfully')
    flash(Markup('Now go to make some \
    <a href="/query"><label class="etichetta">query</label>'))

    return render_template('selectmodel.html', files=files)

