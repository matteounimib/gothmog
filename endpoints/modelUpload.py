import os
from flask import Blueprint, Flask, flash, request, redirect, render_template, session, Response
from flask import current_app as app
from werkzeug.utils import secure_filename
import urllib.request

modelUpload_bp = Blueprint('modelUpload_bp', __name__, template_folder='templates', static_folder='static')

ALLOWED_MODEL = set(['model'])

def allowed_model(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_MODEL


@app.route('/uploadmodel')
def upload_model_form():
    return render_template('uploadmodel.html')


@app.route('/uploadmodel', methods=['POST'])
def upload_model():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No file selected')
            return redirect(request.url)
        if file and allowed_model(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['MODEL_FOLDER'], file.filename))
            flash('Upload completed')
            return redirect(request.url)
        else:
            flash('Allowed file type is model')
            return redirect(request.url)

