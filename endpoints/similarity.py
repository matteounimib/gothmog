import os
from flask import Blueprint, Flask, flash, request, redirect, render_template, session, Markup, Response
from flask import current_app as app
import urllib.request

from libs.query import *


similarity_bp = Blueprint('similarity_bp', __name__, template_folder='templates', static_folder='static')

@app.route('/similarity', methods=['GET'])
def similarity_form():
    # Used to save similarity result
    session['similarity'] = []
    #Save common word in the file temp.txt
    common_file(session['models'][0], session['models'][1])

    return render_template('./similarity.html', models=session['models'])

@app.route('/similarity', methods=['POST'])
def similarity():
    word = request.form.get('autocomplete')
    par = request.form.get('par')
    model = session['models']
    model1 = request.form.get('first_model')
    model2 = request.form.get('second_model')
    
    #Check if the user wrote a word
    if word == '':
        flash('Write a word')
        return render_template('/similarity.html', models=session['models'], sim=session['similarity'])

    # check if par is an int
    try:
        par = int(par)
    except:
        flash('Insert an integer')
        return render_template('/similarity.html', models=session['models'], sim=session['similarity'])

    f = open('./temp.txt', "r")
    commons= f.read()
    commons = commons.split('\n')
    f.close()

    #Check if the word is in commons
    if word in commons:

        if par > len(commons):
            flash('The parameter is bigger than the common words between the two model')
            return render_template('/similarity.html', models=session['models'], sim=session['similarity'])

        result = similar_word(model1, model2, word, par)
        result_string = str(result[0])
        if len(result) > 1:
            for i in result[1:]:
                result_string += ', ' + str(i)

        session['similarity'] += [[model1, model2, word, result_string]]

    else:
        flash('This word is not in common between the two model')

        return render_template('/similarity.html', models=session['models'], sim=session['similarity'])

    return render_template('/similarity.html', models=session['models'], sim=session['similarity'])

