FROM python:3.8-buster
WORKDIR /code
ENV FLASK_APP app.py
ENV FLASK_RUN_HOST 0.0.0.0
COPY requirements.txt requirements.txt
SHELL ["/bin/bash", "-c"]
RUN apt-get update
RUN apt-get install -y gcc python-numpy python-scipy python-virtualenv git
RUN virtualenv -p python3.8 env
RUN source env/bin/activate
RUN pip install git+https://github.com/valedica/gensim.git
RUN git clone https://github.com/valedica/twec
RUN pip install -e ./twec
RUN pip install -r requirements.txt
RUN python -c "import nltk ; nltk.download('punkt') ; nltk.download('stopwords') ; nltk.download('wordnet')"
COPY . .
EXPOSE 5000
CMD ["python", "__init__.py"]
