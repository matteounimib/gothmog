# Gothmog
Gothmog is a semantic analyzer between two different text, which uses the [TWEC](https://github.com/valedica/twec) library. Gothmog is pipeline where the user can configure which analysis he could apply on the two text. The pipeline consist of the preprocessing step, training phase, query phase and displaying results.

<div align="center">
  <img src="static/gothmog.png">
</div>

## Architecture
Gothmog is an application client-server that use the http for the communication. The server side is written in Python with the Flask library and html to generate the web pages.

## Install
After cloning the Gothmog repository you need to install the TWEC library. You need to create a virtual environment because TWEC uses a custom version of the gensim library:

* `git clone https://gitlab.com/matteounimib/gothmog.git`
* `cd ./gothmog`
 * `git clone https://github.com/valedica/twec.git`
 * `virtualenv -p python3.6 env`
* `source env/bin/activate`
* `pip install cython`
* `pip install git+https://github.com/valedica/gensim.git`
* cd in repository
* `pip install -e .`

## How to use
You should cd in the repository and just run the `python __init__.py` command and the server will start. After that, open your favorite browser and go to `127.0.0.1:5000` and you can start with the Gothmog pipeline.

## About
For more information check the [wiki](https://gitlab.com/matteounimib/gothmog/-/wikis/home).


