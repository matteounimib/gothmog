import os
from waitress import serve
from datetime import datetime
from flask import Flask, Response
#from flask import Flask, flash, request, redirect, render_template, send_file, session, Markup, Response
from flask_wtf import FlaskForm
from flask_bootstrap import Bootstrap
import string
import secrets

def create_app():
    SECRET_KEY_LEN = 100
    # app = Flask(__name__)
    app = Flask(__name__, instance_relative_config=False)
    app.config['SECRET_KEY'] = (lambda n: ''.join([string.ascii_letters[secrets.randbelow(len(string.ascii_letters))] for i in range(n)]))(SECRET_KEY_LEN)

    # Upload confi g
    uploadFold = "./uploads"
    modelFold = "./model"
    # 800 MB limit
    sizeLimit = 800*1024*1024
    app.config['UPLOAD_FOLDER'] = uploadFold
    app.config['MODEL_FOLDER'] = modelFold
    app.config['MAX_CONTENT_LENGTH'] = sizeLimit
    #app.config.from_object('config.Config')
    with app.app_context():
        from endpoints import index, uploadCorpus, select, preprocessing, frequency, prepStats, modelSelection, modelUpload, trainModel, query, rank, autocomplete, similarity
        app.register_blueprint(index.index_bp)
        app.register_blueprint(uploadCorpus.upload_bp)
        app.register_blueprint(select.select_bp)
        app.register_blueprint(preprocessing.preprocessing_bp)
        app.register_blueprint(frequency.frequency_bp)
        app.register_blueprint(prepStats.prepStats_bp)
        app.register_blueprint(modelSelection.modelSelection_bp)
        app.register_blueprint(modelUpload.modelUpload_bp)
        app.register_blueprint(trainModel.trainModel_bp)
        app.register_blueprint(query.query_bp)
        app.register_blueprint(rank.rank_bp)
        app.register_blueprint(autocomplete.autocomplete_bp)
        app.register_blueprint(similarity.similarity_bp)
    return app

app = create_app()

# HTTP Errors
@app.context_processor
def send_datetime_obj():
    return dict(time=datetime.now())


@app.errorhandler(404)
def not_found(exc):
    return Response('<h3>Not found</h3>'), 404


@app.errorhandler(400)
def bad_request(exc):
    return Response('<h3>Bad request</h3>'), 400


@app.errorhandler(413)
def too_large(exc):
    return Response('<h3>Selected file is too large</h3>'), 413

@app.after_request
def after_request(response):
    header = response.headers
    header['Access-Control-Allow-Origin'] = '*'
    header['Access-Control-Allow-Headers'] = 'Content-Type, Authorization'
    header['Access-Control-Allow-Methods'] = 'OPTIONS, HEAD, GET, POST, DELETE, PUT'
    return response

# Setup Boostrapp
Bootstrap(app)

# Serve app with waitress
serve(app, host='0.0.0.0', port=5000)
