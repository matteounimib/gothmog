from itertools import islice
from twec.twec import TWEC
from gensim.models.word2vec import Word2Vec
from scipy import spatial
from scipy.spatial.distance import cdist
import numpy as np
import os

# Load selected models
def loadModels(firstM, secondM):
  os.chdir('model')
  model1 = Word2Vec.load(os.path.join(os.getcwd(), firstM))
  model2 = Word2Vec.load(os.path.join(os.getcwd(), secondM))
  os.chdir('..')
  return (model1, model2)

# Global Distance
def global_distance(firstM, secondM, word):
  model1, model2 = loadModels(firstM, secondM)
  return spatial.distance.cosine(model1[word], model2[word])

# Cosine similarity
def cosine_sim(firstM, secondM, word):
  model1, model2 = loadModels(firstM, secondM)
  return 1 - spatial.distance.cosine(model1[word], model2[word])

# Local distance
def local_neighbour_measure(firstM, secondM, word, k, commonWords):
  model1, model2 = loadModels(firstM, secondM)

  try:
    n1 = [w for w, tmp in model1.most_similar(word, topn=k)]
    n2 = [w for w, tmp in model2.most_similar(word, topn=k)]

    common_neighborhood = [w for w in list(set(n1)|set(n2)) if w in commonWords]

  except:
    return 'Are not present {0} common neighbour'.format(k)
  
  s1 = [model1.similarity(word, w) for w in common_neighborhood]
  s2 = [model2.similarity(word, w) for w in common_neighborhood]  

  return spatial.distance.cosine(s1, s2)

# Common Words between two model
def common_words(firstM, secondM):
  model1, model2 = loadModels(firstM, secondM)
  return [i for i in model1.wv.vocab if i in model2.wv.vocab]

# Global distance, as cosine distance of all words
def cos_dis(firstM, secondM, commonWords, n):
  model1, model2 = loadModels(firstM, secondM)
  return {k:spatial.distance.cosine(model1[k], model2[k]) for k in commonWords}

# Abs Consine similarity distance of all words
def cos_sim(firstM, secondM, commonWords, n):
  model1, model2 = loadModels(firstM, secondM)
  return {k:abs(1-spatial.distance.cosine(model1[k], model2[k])) for k in commonWords}

# Local distance of all words
def loc_dis(firstM, secondM, commonWords, nei, n):
  return {k:local_neighbour_measure(firstM, secondM, k, nei, commonWords) for k in commonWords}

# Given a word return the most n similar word between two model 
def similar_word(model1, model2, word, n):
  model1, model2 = loadModels(model1, model2)
  return [i[0] for i in model2.wv.similar_by_vector(model1[word], topn=n)]

def common_file(model1, model2):
  common = common_words(model1, model2)
  common.sort()
  #Save the common words in temp.txt so the browser will not be
  #overloaded
  with open('./temp.txt', 'w+') as f:
      for i in common:
          f.write("%s\n" % i) 
  
  return 0


