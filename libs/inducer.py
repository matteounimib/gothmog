from twec.twec import TWEC
from gensim.models.word2vec import Word2Vec
from scipy import spatial
from scipy.spatial.distance import cdist
import numpy as np
import os

from libs.query import loadModels

#Twec training
def training_twec(first_model, second_model, compass):
	aligner = TWEC(size=30, siter=10, diter=10, workers=4)
	# train the compass: the text should be the concatenation of the text from the slices
	aligner.train_compass(compass, overwrite=True) # keep an eye on the overwrite behaviour
	# now you can train slices and they will be already aligned
	# these are gensim word2vec objects
	slice_one = aligner.train_slice(first_model, save=True)
	slice_two = aligner.train_slice(second_model, save=True)

	first_model_name = os.path.splitext(os.path.basename(first_model))[0]
	second_model_name = os.path.splitext(os.path.basename(second_model))[0]

	model1, model2 = loadModels(first_model_name + '.model', second_model_name + '.model')

	return 0
