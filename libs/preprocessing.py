import re
import nltk
import string
from string import punctuation
import collections
from nltk.stem import WordNetLemmatizer
from nltk.corpus import wordnet

#Remove the stopword in a text. Work only with english language
def stopWordsRemoval(s):
    stopWords = set(nltk.corpus.stopwords.words('english'))
    tokens_ = [nltk.word_tokenize(i) for i in nltk.sent_tokenize(s)]
    tokens = nltk.word_tokenize(s)
    tokens = []
 
    for token_by_sent in tokens_:
        tokens += token_by_sent
    tokens = list(map(lambda w: w.lower(), tokens))
    tokens = list(filter(lambda t: t not in stopWords, tokens))

    words = ""
    for i in tokens:
        words += i + " "
        
    return words

#Remove nonalphanumeric symbol
def nonalphanumeric_removal(text):
    return re.sub(r'\W+', " ", text)

#Remove email in the text
def email_removal(text):
    return re.sub("\S*@\S*\s?", "", text)

#Remove the parenthesis and the symbol inside them
def parenthesis_removal(text):
    return re.sub(r'(\[|\(|\{)[^()]*(\]|\)|\})', '', text)

#Delete the word if its length is less or equal than n
def length_cut(text, n):
    return ' '.join([i.strip() for i in text.split(' ') if len(i.strip()) >= n])

#Apply a given regexp
def manual_regexp(text, regexp):
    return re.sub(regexp, '', text)

#check if the regexp is a well formed formula
def check_reg(reg):
    try:
        re.sub(reg, '', 'test')
        return True
    except:
        return False

#Lemmatize a given text
def lemmatize(text):
    text = nonalphanumeric_removal(text)
    lemmatizer = WordNetLemmatizer()
    sentence_words = nltk.word_tokenize(text)
    words = ""
    for word in sentence_words:
        words += lemmatizer.lemmatize(word, pos='v') + " "

    return words

#Stemming a given text
def stemming(text):
    # aggressive stemming
    text = nonalphanumeric_removal(text)
    lemmatizer = nltk.LancasterStemmer()

    return ' '.join(list(map(lambda x: lemmatizer.stem(x), nltk.word_tokenize(text))))

def punctuation_removal(text):
    return "".join(" " if i in punctuation else i for i in text.strip(punctuation))


# return {word: abs_frequency} of the given text 
def fqCounter(text):
    fq = {}
    words = nltk.word_tokenize(text)
    for i in words:
        if i not in fq.keys():
            fq[i] = 1
        else:
            fq[i] +=1

    return collections.OrderedDict(fq)


# remove all word with frequency < fq from text 
def fqRemove(text, fq):
    fq_pure = fqCounter(text)
    s = sum(fq_pure.values())
    for k,v in list(fq_pure.items()):
        fq_pure[k] = v/s
        if fq_pure[k] < fq:
            del fq_pure[k]

    result = ""
    for i in nltk.word_tokenize(text):
        if i in fq_pure.keys():
            result += i + " "

    return result[:-1]
