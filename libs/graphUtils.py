import numpy as np
import nltk
import collections
import os
import io
from flask import Response
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure

def plot_generic(f):
    fig = f()
    output = io.BytesIO()
    FigureCanvas(fig).print_png(output)

    return Response(output.getvalue(), mimetype='image/png')

# Plot of filesize
def graph_filesize(files, originalData, postPrepData, postFqData, title, ylabel):
    width = 0.16
    fig = Figure()
    x = np.arange(len(files))
    fig.patch.set_alpha(0)
    axis = fig.add_subplot()
    original = axis.bar(x-width, originalData, width, label = "original")
    postPrep = axis.bar(x+width/3, postPrepData, width, label = "preprocessing")
    postFq = axis.bar(x+width*5/3, postFqData, width, label = "frequency removal")
    axis.set_ylabel(ylabel)
    axis.set_title(title)
    axis.set_xticks(x)
    axis.set_xticklabels(list(map(lambda x : x.split('.prep')[0], files)))
    axis.legend()
    def autolabel(rects):
        for rect in rects:
            height = rect.get_height()
            axis.annotate('{}'.format(height),
                        xy=(rect.get_x() + rect.get_width() / 2, height),
                        xytext=(0, 3),  # 3 points vertical offset
                        textcoords="offset points",
                        ha='center', va='bottom')
    autolabel(original)
    autolabel(postPrep)
    autolabel(postFq)
    # fig.tight_layout()

    return fig

# build lists for the total words and total single words plots
def ret_data(folder, files, f):
    processed_files = list(map(lambda x : x.split('.prep')[0], files))
    preprocessed_files = list(map(lambda x : x.split(".fq")[0], files))
    dimF = []
    dimP = []
    dimPf = []
    for o,p,q in list(zip(files, preprocessed_files, processed_files)):
        locationF = os.path.join(folder, o)
        locationP = os.path.join(folder, p)
        locationQ = os.path.join(folder, q)
        dimF.append(f(locationF))
        dimP.append(f(locationP))
        dimPf.append(f(locationQ))

    return (dimPf, dimP, dimF)

# build plot of cumulative words 
def graphPrepare(fq):
    cumulative = []
    fq_pure = list(fq.values())
    s = sum(fq_pure)
    fq_relative = list(map(lambda x:x/s,fq_pure))
    fq_relative.sort(reverse=True)
    cumulative.append(fq_pure[0])
    for i in range(1,len(fq_pure)):
        cumulative.append(cumulative[i-1]+fq_pure[i])

    return (fq_relative, cumulative[::-1])

