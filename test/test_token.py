import sys
sys.path.append('..')
from libs.preprocessing import stopWordsRemoval
from libs.preprocessing import stemming

import unittest

testSuite = unittest.TestSuite()
class test_tokenize(unittest.TestCase):
    def test_no_stopword(self):
        self.assertNotIn(' a', stopWordsRemoval('This string contains a stopword'))

    def test_stopword(self):
        self.assertIn('stopword', stopWordsRemoval('No stopwords here'))

    def test_punct(self):
        self.assertNotIn('UPPERCASE', stopWordsRemoval('THIS STRING IS UPPERCASE'))

    def test_punct(self):
        self.assertIn('!', stopWordsRemoval('This string contains a symbol!'))

    def test_stemming(self):
        self.assertNotIn('programmers', stemming('programmers, coding'))

    def test_stemmingIn(self):
        self.assertIn('program', stemming('programmers, coding'))
if __name__ == '__main__':
    unittest.main()
