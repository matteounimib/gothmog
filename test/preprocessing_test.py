import unittest
import re

testSuite = unittest.TestSuite()
#This test case is not testing the whole nonalphanumeric_removal function 
#but only che regexep
class TestAlphanumeric(unittest.TestCase):

    def test_only_one_word(self):
        self.assertEqual(re.sub(r'\W+', " ", 'hello=?=?'), 'hello ')

    def test_only_one_not_alphanumeric_word(self):
        self.assertEqual(re.sub(r'\W+', " ", '§§§*°*°'), ' ')

    def test_different_symbol(self):
        self.assertEqual(re.sub(r'\W+', " ", '§_§§*_°*°'), ' _ _ ')

    def test_word_with_underscore(self):
        self.assertEqual(re.sub(r'\W+', " ", 'hello_word'), 'hello_word')

#This test case is not testing the whole email_removal function 
#but only che regexep
class TestEmail(unittest.TestCase):

    def test_only_email(self):
        self.assertEqual(re.sub("\S*@\S*\s?","", 'Np@complete.it'), '')

    def test_phrase(self):
        self.assertEqual(re.sub("\S*@\S*\s?","", 'hi my email is mario@rossi.it'), 'hi my email is ')

class TestParenthesis_removal(unittest.TestCase):

    def test_one_parenthesis(self):
        self.assertEqual(re.sub(r'(\[|\(|\{)[^()]*(\]|\)|\})', '', '(hello)'), '')
    
    def test_two_parenthesis(self):
        self.assertEqual(re.sub(r'(\[|\(|\{)[^()]*(\]|\)|\})', '', '(hello) (my)'), ' ')
    
    def test_phares(self):
        self.assertEqual(re.sub(r'(\[|\(|\{)[^()]*(\]|\)|\})', '', 'my name is (mario)'), 'my name is ')
    
    def test_a_lot_of_parenthesis(self):
        self.assertEqual(re.sub(r'(\[|\(|\{)[^()]*(\]|\)|\})', '', 'my name is (mario{rossi}) [!!]'), 'my name is  ')


if __name__ == '__main__':
    unittest.main()