import unittest
import platform
from selenium import webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

#To make this test work you need to add in the .\uploads folder two files txt named:
#first.txt and second.txt
class Test_from_select_txt(unittest.TestCase):

    def setUp(self):
        #If you are on linux uncomment this and comment the next line
        if platform.system() == 'Windows':
            #If geckodriver are in another PATH change it
            self.driver = webdriver.Firefox(executable_path='C:\Program Files\Mozilla Firefox\geckodriver.exe')
        else:
            self.driver = webdriver.Firefox()

    def test(self):
        driver = self.driver
        driver.get('http://127.0.0.1:5000/')

        #Homepage
        driver.find_element_by_link_text('Select').click()
        
        #Select
        select_first =  Select(driver.find_element_by_name('first'))
        select_first.select_by_value('first.txt')        
        select_second = Select(driver.find_element_by_name('second'))
        select_second.select_by_value('second.txt')       
        driver.find_element_by_id('submit').click()

        driver.find_element_by_link_text('preprocessing').click()

        #Preprocessing
        driver.find_element_by_id('Alpha').click()
        driver.find_element_by_id('Pare').click()
        Select(driver.find_element_by_name('ls')).select_by_value('lemmatize')
        driver.find_element_by_id('addregexp').click()
        driver.find_element_by_id('input_text1').send_keys('test')
        driver.find_element_by_id('submit').click()

        driver.find_element_by_link_text('frequency').click()

        #Frequency
        driver.find_element_by_name('minFreq').send_keys('.00000000001')
        driver.find_element_by_id('submit').click()
        
        driver.find_element_by_link_text('Preprocessing Stats').click()

        #Preprocessing Stats
        driver.find_element_by_link_text('Train Model').click()

        #Train model
        driver.find_element_by_link_text('Train the preprocessed file').click()
        driver.find_element_by_id('submit').click()

        driver.find_element_by_link_text('query').click()

        #Query
        driver.find_element_by_id('autocomplete').send_keys('word')
        driver.find_element_by_id('submit').click()
        driver.find_element_by_link_text('Words Rank').click()

        #Word Rank
        driver.find_element_by_id('local').click()
        driver.find_element_by_id('par').send_keys('2')
        driver.find_element_by_id('n').send_keys('5')
        driver.find_element_by_id('submit').click()
        driver.find_element_by_link_text('Word Similarity').click()
        
        #Word Similarity
        #Change the value in select_by_value if the model name change
        Select(driver.find_element_by_name('first_model')).select_by_value('first.txt.prep.model')
        Select(driver.find_element_by_name('second_model')).select_by_value('second.txt.prep.model')
        driver.find_element_by_id('autocomplete').send_keys('word')
        driver.find_element_by_id('par').send_keys('4')
        driver.find_element_by_id('submit').click()

        print('Route from select .txt without error check complete')

    def tearDown(self):
        self.driver.close()
        

class Test_from_select_model(unittest.TestCase):

    def setUp(self):
        #If you are on linux uncomment this and comment the next line
        if platform.system() == 'Windows':
            #If geckodriver are in another PATH change it
            self.driver = webdriver.Firefox(executable_path='C:\Program Files\Mozilla Firefox\geckodriver.exe')
        else:
            self.driver = webdriver.Firefox()


    def test(self):
        driver = self.driver
        driver.get('http://127.0.0.1:5000/')

        #Homepage
        driver.find_element_by_link_text('Select a Model').click()

        #Select Model
        Select(driver.find_element_by_name('first_model')).select_by_value('first.txt.prep.model')
        Select(driver.find_element_by_name('second_model')).select_by_value('second.txt.prep.model')
        driver.find_element_by_id('submit').click()

        driver.find_element_by_link_text('query').click()

        print('Route from select model without error check complete')

    def tearDown(self):
        self.driver.close()


class Test_from_select_txt_with_error(unittest.TestCase):

    def setUp(self):
        #If you are on linux uncomment this and comment the next line
        if platform.system() == 'Windows':
        #If geckodriver are in another PATH change it
            self.driver = webdriver.Firefox(executable_path='C:\Program Files\Mozilla Firefox\geckodriver.exe')
        else:
            self.driver = webdriver.Firefox()


    def test(self):
        driver = self.driver
        driver.get('http://127.0.0.1:5000/')

        #Homepage
        driver.find_element_by_link_text('Select').click()
        
        #Select two equal
        select_first =  Select(driver.find_element_by_name('first'))
        select_first.select_by_value('first.txt')        
        select_second = Select(driver.find_element_by_name('second'))
        select_second.select_by_value('first.txt')       
        driver.find_element_by_id('submit').click()
        assert "Select two different corpus" in driver.page_source

        #Select Right
        select_first =  Select(driver.find_element_by_name('first'))
        select_first.select_by_value('first.txt')        
        select_second = Select(driver.find_element_by_name('second'))
        select_second.select_by_value('second.txt')       
        driver.find_element_by_id('submit').click()

        driver.find_element_by_link_text('preprocessing').click()

        #Preprocessing
        driver.find_element_by_id('Alpha').click()
        driver.find_element_by_id('Pare').click()
        Select(driver.find_element_by_name('ls')).select_by_value('lemmatize')
        driver.find_element_by_id('addregexp').click()
        #Wrong regexp
        driver.find_element_by_id('input_text1').send_keys('£%)%£"(')
        driver.find_element_by_id('submit').click()
        assert 'The £%)%£"(, formula is not WFF.'in driver.page_source
        #Right regexp
        driver.find_element_by_id('Alpha').click()
        driver.find_element_by_id('Pare').click()
        Select(driver.find_element_by_name('ls')).select_by_value('lemmatize')
        driver.find_element_by_id('addregexp').click()
        driver.find_element_by_id('input_text1').send_keys('42')
        driver.find_element_by_id('submit').click()

        driver.find_element_by_link_text('frequency').click()

        #Frequency
        #Not number
        driver.find_element_by_name('minFreq').send_keys('abc')
        driver.find_element_by_id('submit').click()
        assert 'Insert a frequency value in [0,1]'in driver.page_source
        driver.find_element_by_link_text('Frequency').click()
        
        #Right number
        driver.find_element_by_name('minFreq').send_keys('.00000000001')
        driver.find_element_by_id('submit').click()
        driver.find_element_by_link_text('Preprocessing Stats').click()

        #Preprocessing Stats
        driver.find_element_by_link_text('Train Model').click()

        #Train model
        driver.find_element_by_link_text('Train the preprocessed file').click()
        driver.find_element_by_id('submit').click()

        driver.find_element_by_link_text('query').click()

        #Query
        #Not a number in par
        driver.find_element_by_id('Local').click()
        driver.find_element_by_id('par').send_keys('abc')
        driver.find_element_by_id('autocomplete').send_keys('word')
        driver.find_element_by_id('submit').click()
        assert 'Insert an integer'in driver.page_source

        #Not a common word
        driver.find_element_by_id('autocomplete').send_keys('9t2jtoh2')
        driver.find_element_by_id('submit').click()
        assert 'This word is not in common between the two model' in driver.page_source

        #To big par
        driver.find_element_by_id('Local').click()
        driver.find_element_by_id('par').send_keys('424242424242424242')
        driver.find_element_by_id('autocomplete').send_keys('word')
        driver.find_element_by_id('submit').click()
        
        #Right Query
        driver.find_element_by_id('autocomplete').send_keys('word')
        driver.find_element_by_id('submit').click()
        driver.find_element_by_link_text('Words Rank').click()

        #Word Rank
        #Not a number par
        driver.find_element_by_id('local').click()
        driver.find_element_by_id('par').send_keys('abc')
        driver.find_element_by_id('n').send_keys('5')
        driver.find_element_by_id('submit').click()
        assert 'Neighbours number must be an integer number' in driver.page_source

        #Not a number n
        driver.find_element_by_id('local').click()
        driver.find_element_by_id('par').send_keys('2')
        driver.find_element_by_id('n').send_keys('abc')
        driver.find_element_by_id('submit').click()
        assert 'Enter an integer number' in driver.page_source

        #par too big
        driver.find_element_by_id('local').click()
        driver.find_element_by_id('par').send_keys('424242424242')
        driver.find_element_by_id('n').send_keys('2')
        driver.find_element_by_id('submit').click()
        assert 'Are not present 424242424242 common neighbour' in driver.page_source

        #Right Word rank
        driver.find_element_by_id('local').click()
        driver.find_element_by_id('par').send_keys('2')
        driver.find_element_by_id('n').send_keys('5')
        driver.find_element_by_id('submit').click()
        driver.find_element_by_link_text('Word Similarity').click()
        
        #Word Similarity
        #Change the value in select_by_value if the model name change
        #Parameter too much big
        Select(driver.find_element_by_name('first_model')).select_by_value('first.txt.prep.model')
        Select(driver.find_element_by_name('second_model')).select_by_value('second.txt.prep.model')
        driver.find_element_by_id('autocomplete').send_keys('word')
        driver.find_element_by_id('par').send_keys('424242424242424242')
        driver.find_element_by_id('submit').click()
        assert 'The parameter is bigger than the common words between the two model' in driver.page_source

        #Not common word
        Select(driver.find_element_by_name('first_model')).select_by_value('first.txt.prep.model')
        Select(driver.find_element_by_name('second_model')).select_by_value('second.txt.prep.model')
        driver.find_element_by_id('autocomplete').send_keys('hfwihge')
        driver.find_element_by_id('par').send_keys('4')
        driver.find_element_by_id('submit').click()
        assert 'This word is not in common between the two model' in driver.page_source

        #Right Similarity
        Select(driver.find_element_by_name('first_model')).select_by_value('first.txt.prep.model')
        Select(driver.find_element_by_name('second_model')).select_by_value('second.txt.prep.model')
        driver.find_element_by_id('autocomplete').send_keys('word')
        driver.find_element_by_id('par').send_keys('4')
        driver.find_element_by_id('submit').click()

        print('Route from select .txt with error check complete')

    def tearDown(self):
        self.driver.close()
        

if __name__ == "__main__":
    unittest.main()