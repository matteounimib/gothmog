import sys
sys.path.append('..')
from libs.preprocessing import *
import unittest

testSuite = unittest.TestSuite()
class test_tokenize(unittest.TestCase):
    def test_remove_frequency(self):
        self.assertNotIn('remove', fqRemove("test test test test test remove", 1/5))

    def test_not_remove_frequency(self):
        self.assertIn('test', fqRemove("test test test test test remove", 1/5))

    def test_frequency_counter(self):
        self.assertEqual(fqCounter("test test string string")["test"], 2)

if __name__ == '__main__':
    unittest.main()
